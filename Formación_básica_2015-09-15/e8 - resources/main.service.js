angular.module('testApp').factory('mainService', ['$log', '$resource', function($log, $resource){
	return $resource('http://localhost:3000/people/:id', {id:'@id'}, {
        getAll: {
            url: 'http://localhost:3000/people',
            method: 'GET',
            cache: false,
            isArray: true
        }
    });
}]);