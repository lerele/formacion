angular.module('testApp').controller('mainCtrl', ['$timeout', 'mainService', function($timeout, mainService){
	var scope = this;

	scope.loading = true;
	$timeout(function(){
		// Retrasar durante 2 segundos
		scope.people = mainService.getAll(function(data){
			console.log(scope.people === data);
			$timeout(function(){
				scope.people.unshift(angular.copy(data[data.length-1]));
				scope.loading = false;
			},2000);
		});
		scope.firstPerson = mainService.get({id:1});
	},2000);
}]);