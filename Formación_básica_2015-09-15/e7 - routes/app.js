angular.module('testApp', ['ui.router'])
.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('start', {
      url: "/",
      templateUrl: "home.html",
      controller: function($scope){
      	$scope.message = 'Esta es mi p�gina de ejemplo';
      }
    })
    .state('contact', {
      url: "/contact",
      templateUrl: "contact.html",
      controller: 'mainCtrl as vm'
    });
});