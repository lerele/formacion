angular.module('testApp').factory('mainService', ['$log', function($log){
	var people = [{name: 'Juan', age: 20},{name:'Pepe', age: 32}, {name: 'Lola', age: 54}];
	var service = {};
	service.getPeople = function(filter){
		$log.log("Obteniendo personas...",'filtro:', filter);
		return people.filter(function(value, pos, arr){
			if (!filter) return true;
			if (filter.maxAge && filter.maxAge < value.age) return false;
			if (filter.minAge && filter.minAge > value.age) return false;
			return true;
		});
	};
	service.getPerson = function(personName){
		return people.find(function(e){return e.name == personName;});
	}
	return service;
}]);