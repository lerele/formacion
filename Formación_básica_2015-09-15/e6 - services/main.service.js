angular.module('testApp').factory('mainService', ['$log', function($log){
	var people = [{name: 'Juan', age: 20},{name:'Pepe', age: 32}, {name: 'Lola', age: 54}];
	var service = {
		getPeople: getPeople,
		mock: { id: 33, name: 'pepe'},
		getMock: getMock
	};
	
	function getPeople(filter){
		$log.log("Obteniendo personas...",'filtro:', filter);
		return people.filter(function(value, pos, arr){
			if (!filter) return true;
			if (filter.maxAge && filter.maxAge < value.age) return false;
			if (filter.minAge && filter.minAge > value.age) return false;
			return true;
		});
	};
	
	function getMock(){
		//fjalksfj�alskjdfkl�as asdfjakl�s
		// jfaskl�fj�aklsdfj�alsdkf
		return service.mock;
	}
	return service;
}]);