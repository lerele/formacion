angular.module('testApp').controller('mainCtrl', ['mainService', function(mainService){
	//$log.log(mainService);
	var scope = this;
	scope.people = mainService.getPeople();
	scope.search = function(){
		scope.people = mainService.getPeople(scope.filter);
	}
}]);