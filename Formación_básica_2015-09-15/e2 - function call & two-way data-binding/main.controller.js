angular.module('testApp', []).controller('mainCtrl', [function(){
	var scope = this;
	scope.message = 'Hello world';

	scope.reset = function(){
		scope.message = '';
	};
}]);