# Ejercicio 2 - Bower

En este ejercicio, usamos bower para descargar las dependencias web.

Bower es similar a npm, solo que el fichero que utiliza se llama ``bower.json`` y la carpeta donde se instalan se llama ``bower_components``.

Para el resto de dependencias, se sigue utilizando npm.

Se puede añadir un script en package.json para ejecutar automáticamente ``bower install``.