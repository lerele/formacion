# Ejercicio 7 - Unit Testing HTTP calls

Mediante el servicio $httpBackend podemos simular el fin de una llamada ajax.

### Uso
```js
$httpBackend.expectGET(url).respond(mockObj);
... // Llamada al servidor
$httpBackend.flush();
// Comprobar condiciones
```