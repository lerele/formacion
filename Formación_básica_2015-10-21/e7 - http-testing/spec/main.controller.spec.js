describe('Controller: mainCtrl', function () {
	
	beforeEach(module('testApp'));
	
	var ctrl, $httpBackend;

	beforeEach(inject(function ($controller, _$httpBackend_) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('http://localhost:3000/people')
			.respond([{id:1, name: 'FakeName', age: 80}]);
		ctrl = $controller('mainCtrl');
	}));

	it('return all people', function(){
		expect(ctrl.people).toBeFalsy();
		ctrl.search();
		expect(ctrl.people).toBeFalsy();
		$httpBackend.flush();
		expect(ctrl.people).not.toBeFalsy();
		expect(angular.equals(ctrl.people, [{id:1,name:'FakeName',age:80}])).toBe(true);
	});

	afterEach(function() {
		// Ensure that all expects set on the $httpBackend
		// were actually called
		$httpBackend.verifyNoOutstandingExpectation();
		// Ensure that all requests to the server
		// have actually responded (using flush())
		$httpBackend.verifyNoOutstandingRequest();
	});
});
