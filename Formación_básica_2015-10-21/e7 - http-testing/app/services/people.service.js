angular.module('testApp').factory('PeopleService', ['$log', '$resource', function($log, $resource){
	return $resource('http://localhost:3000/people/:id', {id: '@id'});
}]);