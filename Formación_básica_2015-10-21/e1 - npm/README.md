# Ejercicio 1 - NPM example
En este ejercicio vemos c�mo podemos usar npm para descargar la dependencia de angular.js.

### Comandos �tiles
  - ``npm install`` - Descarga las dependencias descritas en el fichero package.json en la carpeta node_modules (instalaci�n local).
  - ``npm install angular-resources`` - Descarga la librer�a angular-resources en la carpeta node_modules.
  - ``npm install angular-resources --save`` - A�ade la dependencia al fichero package.json y la instala de forma local.
  - ``npm install angular-mocks --save-dev``- A�ade la dependencia a la secci�n de dependencias de desarrollo (tests) y la instala de forma local.
  - ``npm install -g karma-cli``- Instala de forma global el paquete karma-cli. En windows el directorio de instalaci�n es *%APPDATA%/npm*.
  
### Librer�as t�picas instalaci�n global ``npm install -g <package>``
En general, todo paquete que tenga un ejecutable es candidato a una instalaci�n global.

 - ``bower`` - Gestor de dependencias para web.
 - ``grunt-cli`` - Grunt runner. Permite ejecutar grunt.cmd (en el PATH) y mantener distintas versiones por cada proyecto.
 - ``karma-cli`` - Karma test runner. Permite ejecutar karma.cmd (en el PATH) y mantener distintas versiones por cada proyecto.
 - ``yo`` - Generador de c�digo yeoman.
 - ``rimraf`` - Herramienta para borrar directorios. �til para carpetas anidadas que producen rutas demasiado largas y windows no permite borrar.
 - ``json-server`` - Servidor simple RestFul con persistencia a fichero json.
