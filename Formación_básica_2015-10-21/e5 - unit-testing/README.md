# Ejercicio 5 - Unit Testing

Hay que diferenciar entre Karma (test runner) y Jasmine (test framework).

 1. Se instalan las dependencias del fichero ``package.json``.
 2. El fichero ``karma.conf.js`` define la configuración de los tests los ficheros que se testearán.
 3. ``angular-mocks`` se encarga de mockear los servicios típicos de angularjs para acelerar y simplificar los tests.
