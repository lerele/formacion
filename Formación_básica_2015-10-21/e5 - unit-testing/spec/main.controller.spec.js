describe('Controller: mainCtrl', function () {
	// Instanciar una nueva versión del módulo antes de cada test
	beforeEach(module('testApp'));
	
	var ctrl;
	// Antes de cada test unitario, crear una nueva instancia del controller
	beforeEach(inject(function ($controller) {
		ctrl = $controller('mainCtrl');
	}));

	it('should start with a Hello world message', function(){
		expect(ctrl.message).toBe('Hello world');
	});

	it('should reset message', function(){
		ctrl.message = 'Something';
		ctrl.reset();
		expect(ctrl.message).toBeFalsy();
		expect(ctrl.message).not.toBe(0);
	});

	it('should say Hello', function(){
		ctrl.sayHello();
		expect(ctrl.message).toBe('Hello');
	});

	// Este test se ignora
	xit('should not be tested, not implemented yet', function(){
		expect(ctrl.doSomethingAmazing).not.toBe(undefined);
	});
	
	// Este test ignora los errores tras ejecutar pending()
	it('shold not be tested with action = "somethingNotDone"', function(){
		var action = 'somethingNotDone';
		if (action === 'done'){
			var n = NaN;
			expect(n).not.toBe(n); // test OK
		} else {
			expect(ctrl.somethingNotImplementedYet).not.toBe(undefined); // test FAIL
			pending();
		}
	})
});
