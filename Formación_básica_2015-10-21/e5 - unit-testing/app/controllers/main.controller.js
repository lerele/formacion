angular.module('testApp').controller('mainCtrl', ['MainService', function(MainService){
	var scope = this;
	scope.message = 'Hello world';

	scope.reset = function(){
		scope.message = MainService.sayNothing();
	};
	
	scope.sayHello = function(msg){
		scope.message = MainService.sayHello(msg);
	}
}]);