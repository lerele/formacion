# Ejercicio 6 - Spies

Un spy sobreescribe un método con un proxy. Se pueden utilizar espías para mockear servicios.

### Uso

- ``spyOn(MainService, 'methodName')`` sustituye el servicio con un proxy que intercepta la llamada al método.

De esta manera, podemos controlar si se ha llamado al método mediante instrucciones como ``expect(MainService.sayNothing).toHaveBeenCalled();`` o sustituir su funcionalidad con nuestra propia implementación:

- Indicando directamente el valor a devolver en adelante:
```js
spyOn(MainService, 'methodName').and.returnValue(true);
```
- O implementando directamente la nueva función a invocar.
```js
spyOn(MainService, 'methodName').and.callFake(function() {
	console.log('interceptando funcion.')
  return true;
});
```
