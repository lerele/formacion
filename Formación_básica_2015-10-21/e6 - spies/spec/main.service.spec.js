describe('Service: MainService', function () {
	// Instanciar una nueva versión del módulo antes de cada test
	beforeEach(module('testApp'));
	
	var service;
	// Antes de cada test unitario, crear una nueva instancia del controller
	beforeEach(inject(function (MainService) {
		service = MainService;
	}));

	it('should say nothing', function () {
		var nothing = service.sayNothing();
		expect(nothing).toBe('');
	});

	it('should say Hello', function () {
		expect(service.sayHello()).toBe('Hello');
	});

	it('should say Hello to John', function () {
		expect(service.sayHello('John')).toBe('Hello John');
	});

});