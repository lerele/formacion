describe('Controller: mainCtrl', function () {
	
	beforeEach(module('testApp'));
	
	var ctrl, MainService;

	beforeEach(inject(function ($controller, _MainService_) {
		MainService = _MainService_;
		ctrl = $controller('mainCtrl');
	}));

	it('should start with a Hello world message', function(){
		expect(ctrl.message).toBe('Hello world');
	});

	it('should reset message', function(){
		spyOn(MainService, 'sayNothing').and.callThrough();
		ctrl.reset();
		expect(MainService.sayNothing).toHaveBeenCalled();
		expect(MainService.sayNothing.calls.count()).toBe(1);
		expect(ctrl.message).toBe('');
	});

	it('should reset message with a null value', function(){
		spyOn(MainService, 'sayNothing').and.returnValue(null);
		ctrl.reset();
		expect(MainService.sayNothing).toHaveBeenCalled();
		expect(MainService.sayNothing.calls.count()).toBe(1);
		expect(ctrl.message).toBe(null);
	});

});
