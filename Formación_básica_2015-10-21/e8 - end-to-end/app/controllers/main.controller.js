angular.module('testApp').controller('mainCtrl', ['PeopleService', function(PeopleService){
	var scope = this;
	scope.search = function(){
		scope.error = '';
		scope.person = null;
		PeopleService.query().$promise.then(function(data){
			scope.people = data;
		});
	};
	scope.detail = function(id){
		scope.error = '';
		scope.person = null;
		PeopleService.get({id: id}).$promise.then(function(data){
			scope.person = data;
		}, function(err){
			scope.error = err.statusText;
		});
	}
}]);