angular.module('testApp').factory('PeopleService', ['$log', '$resource', function($log, $resource){
	return $resource('api/people/:id', {id: '@id'});
}]);