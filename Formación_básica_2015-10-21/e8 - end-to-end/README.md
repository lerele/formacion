# Ejercicio 8 - End-to-end testing

Este ejercicio, muestra cómo arrancar un test con protractor.

### Instalación
1. ``npm install -g protractor``
2. ``webdriver-manager update``

### Uso
1. ``json-server db.json``
2. ``grunt serve``
3. ``webdriver-manager start``
4. ``protractor``