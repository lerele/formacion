/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    connect: {
      site: {
        options: {
            port: 8000,
            base: '.',
            keepalive: true,
            middleware: function (connect, options, middlewares) {
                middlewares.unshift(require('grunt-connect-proxy/lib/utils').proxyRequest);
                return middlewares;
            }
        }
      },
      server: {
        proxies: [{
          context: '/api',
          host: 'localhost',
          port: 3000,
          rewrite: {
            '^/api' : '/'
          }
        }]
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-connect-proxy');


  grunt.registerTask('serve', ['configureProxies:server', 'connect:site']);

};