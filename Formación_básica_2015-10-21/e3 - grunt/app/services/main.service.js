angular.module('testApp').factory('MainService', ['$log', function($log){
	var service = {
		sayNothing : sayNothing,
		sayHello : sayHello
	};
	function sayNothing(){
		return '';
	}
	function sayHello(msg){
		var message = msg ? 'Hello '+msg : 'Hello';
		$log.log(message);
		return message;
	}
	return service;
}]);