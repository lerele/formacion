# Ejercicio 3 - Grunt

Grunt es el gestor de tareas de Node. Permite invocar diferentes tareas de manera agrupada mediante comandos.
Entre las tareas típicas que podemos realizar, se encuentran:

- Servir ficheros (con capas extras para recarga automática, configurar proxies, sincronización...).
- Minimización de ficheros HTML, JS, CSS, imagenes...
- Concatenación de ficheros JS
- Uglify
- Testing
- Compilado para producción (modificación de ficheros -dev ribbon-).

### Uso

1. Instalación mediante npm. ``npm install <grunt-package> --save-dev``
2. Carga en el fichero Gruntfile.js. ``grunt.loadNpmTasks('grunt-<package>');``
3. Configuración.
 ```js
	grunt.initConfig({
        <package> : { /* Configuración del paquete aquí */}
        ...
    });
 ```
 4. Registrar tarea. ``grunt.registerTask('taskname', ['<package>']);``
 5. Invocar tarea. ``$ grunt taskname``