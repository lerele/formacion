# Ejercicio 4 - Grunt proxy

Este ejercicio muestra una configuración simple de un servidor que sirve recursos estáticos y arranca un proxy a todas las llamadas que tengan el patrón ``/api``.

### Uso
1. ``npm install -g grunt-cli`` - Instala el cliente de grunt.
2. ``npm install -g json-server`` - Instala el servidor RestFul de una base de datos de fichero JSON.
3. ``json-server db.json`` - Arranca el servidor RestFul con la base de datos de fichero ``db.json`` en el puerto 3000.
4. ``grunt serve`` - Arranca el servidor grunt con proxy ``localhost:8000/api -> localhost:3000/``